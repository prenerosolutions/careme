-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 21, 2020 at 12:17 PM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `daresni`
--

-- --------------------------------------------------------

--
-- Table structure for table `student_sign_up`
--

CREATE TABLE `student_sign_up` (
  `student_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `country_code` varchar(20) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `flat` varchar(200) NOT NULL,
  `house` varchar(200) NOT NULL,
  `road` varchar(200) NOT NULL,
  `block` varchar(200) NOT NULL,
  `area` varchar(200) NOT NULL,
  `country` varchar(50) NOT NULL,
  `image` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_sign_up`
--

INSERT INTO `student_sign_up` (`student_id`, `first_name`, `last_name`, `email`, `nationality`, `password`, `gender`, `dob`, `country_code`, `contact_number`, `flat`, `house`, `road`, `block`, `area`, `country`, `image`) VALUES
(1, 'Saqib', 'Jutt', 'saqib7t2@gmail.com', 'php', '123456', 'php', '2020-12-23', 'php', '03027750114', '2214', '1212', '123', '45', 'Faisalbad', 'Pakistan', '11707862_141169702885063_2533119962301443873_n.jpg'),
(2, 'Saqib', 'Jutt', 'saqib7t2@gmail.com', 'php', '1234', 'php', '2020-12-08', 'php', '03027750114', '45544', '5455', '65', '645', 'faisalabad', 'P', '2.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `student_sign_up`
--
ALTER TABLE `student_sign_up`
  ADD PRIMARY KEY (`student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student_sign_up`
--
ALTER TABLE `student_sign_up`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
