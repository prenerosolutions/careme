<!DOCTYPE html>
<html>
<head>
	<title>Student Registration | Careme Tutor </title>
	<style type="text/css">
		form{
			width: 600px;
			margin: auto;
			background: gray;
		}
		td{
			margin: 50px;
			padding: 20px;
		}
		input{
			width: 250px;
			height: 40px;
		}
		select{
			width: 250px;
			height: 40px;
		}
	</style>
</head>
<body>

	<?php
$error = Null;                   
    //Connect to the database
    include "connect.php";

if(isset($_POST['submit'])){

				//Get form data
				$first_name = $_POST['s_first_name'];
				$last_name = $_POST['s_last_name'];
				$email = $_POST['s_email'];
				$nationality = $_POST['s_nationality'];
				$password = $_POST['s_password'];
				$c_password = $_POST['s_confirm_password'];
				$gender = $_POST['s_gender'];
				$dob = $_POST['s_dob'];
				$country_code = $_POST['s_country_code'];
				$contact = $_POST['s_contact'];
				$flat = $_POST['s_flat'];
				$house = $_POST['s_house'];
				$road = $_POST['s_road'];
				$block = $_POST['s_block'];
				$area = $_POST['s_area'];
				$country = $_POST['s_country'];

			$dir="upload/student/";
				$image=$_FILES['file']['name'];
				$temp_name=$_FILES['file']['tmp_name'];

				if($image!="")
				{
					if(file_exists($dir.$image))
					{
					$image= time().'_'.$image;
					}

					$fdir= $dir.$image;
					move_uploaded_file($temp_name, $fdir);
				}


  if (strlen($first_name) < 1) {
    $error = "<p>Your username must be at least 1 charactors</p>";
  }elseif ($c_password != $password) {
    $error .= "<p>Your Password do not match</p>";
  }else{
    //Form is valid
    $sql = "SELECT * FROM student_sign_up WHERE email= '$email' ";
                  $result = $con->query($sql);


                  if ($result->num_rows > 0) {
                    $error = "Email already used. Try with other email.";
                  }else{

   // $mysqli = NEW mysqli('localhost', 'root', '', 'daresni');

    //Sanitize Form Data

    // $user = $con->real_escape_string($user);
    // $pass = $con->real_escape_string($pass);
    // $pass2 = $con->real_escape_string($pass2);
    // $email = $con->real_escape_string($email);
    // $phone = $con->real_escape_string($phone);
    // $address = $con->real_escape_string($address);



    //Insert Accojnt iti the database
//    $pass = md5($pass);
    $insert = $con->query("INSERT INTO student_sign_up(first_name, last_name, email, nationality, password, gender, dob, country_code, contact_number, flat, house, road, block,area, country, image)VALUES('$first_name', '$last_name', '$email','$nationality', '$password', '$gender', '$dob', '$country_code', '$contact', '$flat', '$house', '$road', '$block', '$area', '$country', '$image')");

    if ($insert) {
    echo '<script>alert("Your user has been added into the record");</script>';
     }else{
      echo $con->error;
    }

  }
}
}

?>



	<div>
		<form method="post" action="#" enctype="multipart/form-data" name="Sign up">
			<table>
				<tbody>
					<tr>
						<td><label>First Name*</label> <input type="text" name="s_first_name" required="required"></td>

						<td><label>Last Name*</label> <input type="text" name="s_last_name" required=""></td>
					</tr>

					<tr>
						<td><label>Email*</label> <input type="text" name="s_email" placeholder="Email" required=""></td>

						<td><label>Nationality*</label> 
							<select name="s_nationality" id="select">
                
				            	<option value="php"> php table</option>
				              
				           	</select>
						</td>
					</tr>

					<tr>
						<td><label>Password*</label> <input type="password" name="s_password"></td>

						<td><label>Confirm Password*</label> <input type="password" name="s_confirm_password"></td>
					</tr>

					<tr>
						<td><label>Gender*</label> 
							<select name="s_gender" id="select">
                
				            	<option value="php"> php table</option>
				              
				           	</select>
						</td>

						<td><label>Date of Birth*</label> <input type="date" name="s_dob"></td>
					</tr>


					<tr>
						<td><label>Country Code*</label> 
							<select name="s_country_code" id="select">
                
				            	<option value="php"> php table</option>
				              
				           	</select>
						</td>

						<td><label>Contact No*</label> <input type="text" name="s_contact"></td>
					</tr>

					<tr>
						<td><label>Flat No*</label> <input type="text" name="s_flat"></td>

						<td><label>Villa/House/Building No*</label> <input type="text" name="s_house"></td>
					</tr>

					<tr>
						<td><label>Road No*</label> <input type="text" name="s_road"></td>

						<td><label>Block No*</label> <input type="text" name="s_block"></td>
					</tr>

					<tr>
						<td><label>Area*</label> <input type="text" name="s_area"></td>

						<td><label>Country*</label> <input type="text" name="s_country"></td>
					</tr>
					
					<tr>
						<td ><label>Profie Picture*</label> 
							<label> <input type="file" name="file" id="fileField" required/> </label>
						</td>
					</tr>

					<tr>
						<td colspan="2"><label>Are you looking for a Tutor/Coach for yourself?</label> </td>
						
					</tr>
					
					<tr>
						<td colspan="2">
							<input type="submit" name="submit" value="REGISTER NOW" style="background: red; color: white; width: 550px; height: 50px; line-height: 50px; margin: auto; ">
						</td>
						<?php echo $error ?>
					</tr>
				</tbody>
			</table>
		</form>
	</div>

</body>
</html>